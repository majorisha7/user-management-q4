import React, { useState, useContext } from 'react';
import { GlobalContext } from "../context/GlobalState";
import { v4 as uuid } from "uuid";
import { Link, useHistory } from "react-router-dom";
import {
  Form,
  FormGroup,
  Label,
  Input,
  Button
} from "reactstrap";

export const NewUser = () => {
  const [name, setName] = useState('');
  const { addUser } = useContext(GlobalContext);
  const history = useHistory();

  const onSubmit = (e) => {
    e.preventDefault();
    const newUser = {
      id: uuid(),
      name
    }
    addUser(newUser);
    history.push("/");
  }

  const onChange = (e) => {
    setName(e.target.value);
  }

  return (
    <Form onSubmit={onSubmit}>
      <FormGroup>
        <Label><h2>Create New User</h2></Label>
        <Input type="text" value={name} onChange={onChange} name="name" placeholder="Enter Name" required></Input>
      </FormGroup><br />
      <FormGroup>
        <Input type="text" value={name} onChange={onChange} name="name" placeholder="Enter Surname" required></Input>
      </FormGroup><br />
      <FormGroup>
        <Input type="text" value={name} onChange={onChange} name="name" placeholder="Enter UserName" required></Input>
      </FormGroup><br />
      <Button type="submit">Create</Button>
      <Link to="/" className="btn btn-danger ml-2">Back</Link>
    </Form>
  )
}