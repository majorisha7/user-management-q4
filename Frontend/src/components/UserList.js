import React, { useContext } from 'react';
import {Table} from 'reactstrap';
import {Link} from "react-router-dom";
import {
  Button
} from "reactstrap";

export const UserList = () => {
  return (
    <Table striped>
    <thead>
      <tr>
        <th>#</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Username</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">1</th>
        <td>Seza</td>
        <td>Demirbas</td>
        <td>@majorisha7</td>
        <tr>
              <div>
              <Link className="btn btn-warning mr-1" to="/edit/1">Edit</Link>
              <Button color="danger">Delete</Button>
              </div>
              </tr>
      </tr>
    </tbody>
  </Table>
  
  )
}