import React from 'react'
import {Link} from 'react-router-dom';
import{
Form,
FormGroup,
Label,
Input,
Button
} from 'reactstrap';
export const EditUser = () => {
    return (
        <Form>
        <FormGroup>
            <Label><h2>Edit User</h2></Label>
            <Input type="text" placeholder="Enter Name" ></Input>
        </FormGroup>
        <br />
        <FormGroup>
            <Input type="text" placeholder="Enter Surname" ></Input>
        </FormGroup>
        <br />
        <FormGroup>
            <Input type="text" placeholder="Enter Username" ></Input>
        </FormGroup>
        <br />
  
        <Button type="create">Save</Button>
        <Link to= "/" className="btn btn-danger m1-2">Back</Link>
    </Form>
    
    )
}
